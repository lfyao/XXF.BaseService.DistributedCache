﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.DistributedCache.Storage;

namespace XXF.BaseService.DistributedCache.LoadBalance
{
    public interface IBalance
    {
        string ChooseServer(List<string> serviceconfiglist, string key);
    }
}
